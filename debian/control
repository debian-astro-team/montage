Source: montage
Section: science
Priority: optional
Maintainer: Debian Astro Team <debian-astro-maintainers@lists.alioth.debian.org>
Uploaders: Gijs Molenaar <gijs@pythonic.nl>, Ole Streicher <olebole@debian.org>
Build-Depends: cython3,
               debhelper-compat (= 13),
               dh-python,
               libbz2-dev,
               libcfitsio-dev,
               libfreetype-dev,
               libjpeg-dev,
               libwcstools-dev,
               python3-all-dev,
               python3-jinja2,
               python3-setuptools
Standards-Version: 4.7.0
Homepage: http://montage.ipac.caltech.edu/
Vcs-Browser: https://salsa.debian.org/debian-astro-team/montage
Vcs-Git: https://salsa.debian.org/debian-astro-team/montage.git

Package: montage
Architecture: any
Depends: fonts-freefont-ttf, ${misc:Depends}, ${shlibs:Depends}
Description: Toolkit for assembling FITS images into mosaics
 Montage is a toolkit for assembling astronomical images into custom mosaics.
 .
 It uses algorithms that preserve the calibration and positional (astrometric)
 fidelity of the input images to deliver mosaics that meet user-specified
 parameters of projection, coordinates, and spatial scale. It supports all
 projections and coordinate systems in use in astronomy.
 .
 It contains independent modules for analyzing the geometry of images on the
 sky, and for creating and managing mosaics; these modules are powerful tools
 in their own right and have applicability outside mosaic production, in areas
 such as data validation.

Package: montage-gridtools
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Recommends: htcondor, montage
Description: Create files to run montage on the grid
 Montage is a toolkit for assembling astronomical images into custom mosaics.
 .
 This package contains modules that can generate a Directed Acyclic Graph
 (DAG) representation of the mosaicking process for a set of on-line surveys
 (e.g., 2MASS). This information is fed to the Pegasus software from ISI (the
 Information Sciences Institute), which produces processing plans for Condor
 (or Condor-G on the Teragrid).
 .
 These modules should be considered prototypes: they have been used
 extensively but have not been subject to the same rigorous testing to which
 the core modules have been subjected. Users should contact Montage
 (montage@ipac.caltech.edu) before employing them to verify limitations in
 their use and changes in the interfaces.

Package: python3-montagepy
Architecture: any
Section: python
Depends: fonts-freefont-ttf,
         ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends}
Description: Python toolkit for assembling FITS images into mosaics
 MontagePy is a toolkit for assembling astronomical images into custom mosaics.
 .
 It uses algorithms that preserve the calibration and positional (astrometric)
 fidelity of the input images to deliver mosaics that meet user-specified
 parameters of projection, coordinates, and spatial scale. It supports all
 projections and coordinate systems in use in astronomy.
 .
 It contains independent modules for analyzing the geometry of images on the
 sky, and for creating and managing mosaics; these modules are powerful tools
 in their own right and have applicability outside mosaic production, in areas
 such as data validation.
