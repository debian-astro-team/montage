.TH MFITPLANE 1 "Dec 2016" "Montage 5" "Montage"
.SH NAME
mFitplane \- Use least squares to fit a plane to an image

.SH SYNOPSIS
mFitplane [\-b \fIborder\fP] [\-d \fIlevel\fP] [\-s \fIstatusfile\fP] in.fits

.SH DESCRIPTION
Uses least squares to fit a plane (excluding outlier pixels) to an image. It is used on the difference images generated using \fBmDiff\fP or \fBmDiffExec\fP.

.SH OPTIONS
.TP
\-b \fIborder\fP
Number of border pixels to ignore at edges of image.
.TP
\-d \fIlevel\fP
Turns on debugging to the specified level (1\-3).
.TP
\-l
Level only: don't fit a slope; just get a zero level.
.TP
\-s \fIstatusfile\fP
Output and errors are written to \fIstatusfile\fP instead of stdout.

.SH ARGUMENTS
.TP
in.fits
Input FITS file is a difference file between two other FITS files, as can be generated using \fBmDiff\fP.

.SH RESULT
The plane fit to the image is of the form:



\fB
fit = \fIA\fP*x + \fIB\fP*y + \fIC\fP\fP
.PP
where x is the "horizontal" pixel offset from CRPIX1 in the FITS header
and y is the "vertical" offset from CRPIX2.  Since all reprojected images
use the same reference pixel, this is a uniform definition for x,y for the
whole image set.
.PP
The fitting is a simple least squares, with an iterative loop to exclude
points more than 2 sigma (sigma defined as the rms from the previous
loop).
.PP
The \fIrms\fP output is the value computed above.  The \fIxmin, ... ymax\fP output
correspond to the range of x,y values found while fitting.  The rms is for
information only but the \fIxmin, ... ymax\fP are used in the background fitting
(mBgModel).  \fIcrpix1\fP and \fIcrpix2\fP are the reference pixels for each axis, as determined from the FITS header.

.SH MESSAGES
.TP
OK
[struct stat="OK", a=\fIA\fP, b=\fIB\fP, c=\fIC\fP, crpix1=\fIcrpix1\fP, crpix2=\fIcrpix2\fP, xmin=\fIxmin\fP, xmax=\fIxmax\fP, ymin=\fIymin\fP, ymax=\fIymax\fP, xcenter=\fIxcenter\fP, ycenter=\fIycenter\fP, npixel=\fInpixel\fP, rms=\fIrms\fP, boxx=\fIboxx\fP, boxy=\fIboxy\fP, boxwidth=\fIboxwidth\fP, boxheight=\fIboxheight\fP, boxang=\fIboxang\fP]
.TP
ERROR
Argument to \-b (\fIstring\fP) cannot be interpreted as an integer
.TP
ERROR
Cannot open status file: \fIstatusfile\fP
.TP
ERROR
Image file \fIfilename\fP missing or invalid FITS
.TP
ERROR
\fIFITS library error\fP
.TP
ERROR
\fIgeneral error\fP
.TP
WARNING
Too few pixels to fit
.TP
WARNING
Too few pixels for bounding box
.TP
WARNING
Singular Matrix-1
.TP
WARNING
Singular Matrix-2
.TP
WARNING
allocation failure in ivector()

.SH EXAMPLES
.TP
$ mFitplane 2mass-atlas-990322n-j0640266.fits
[struct stat="OK", a=\-8.9198e-11, b=\-2.54173e-10, c=5.24277e-05, crpix1=808, crpix2=212.5, xmin=\-808, xmax=167, ymin=\-210.5, ymax=638.5, xcenter=\-320.871, ycenter=213.014, npixel=314299, rms=0.393003, boxx=\-320.92, boxy=212.424, boxwidth=835.421, boxheight=509.274, boxang=\-29.5537]

.SH BUGS
The drizzle algorithm has been implemented but has not been tested
in this release.
.PP
If a header template contains carriage returns (i.e., created/modified
on a Windows machine), the cfitsio library will be unable to read it
properly, resulting in the error: [struct stat="ERROR", status=207,
msg="illegal character in keyword"]
.PP
It is best for the background correction algorithms if the area
described in the header template completely encloses all of the input
images in their entirety. If parts of input images are "chopped off"
by the header template, the background correction will be affected. We
recommend you use an expanded header for the reprojection and
background modeling steps, returning to the originally desired header
size for the final coaddition. The default background matching assumes
that there are no non-linear background variations in the individual
images (and therefore in the overlap differences). If there is any
uncertainty in this regard, it is safer to turn on the "level only"
background matching (the "\-l" flag in mBgModel.

.SH COPYRIGHT
2001-2015 California Institute of Technology, Pasadena, California
.PP
If your research uses Montage, please include the following
acknowledgement: "This research made use of Montage. It is funded by
the National Science Foundation under Grant Number ACI-1440620, and
was previously funded by the National Aeronautics and Space
Administration's Earth Science Technology Office, Computation
Technologies Project, under Cooperative Agreement Number NCC5-626
between NASA and the California Institute of Technology."
.PP
The Montage distribution includes an adaptation of the MOPEX algorithm
developed at the Spitzer Science Center.
