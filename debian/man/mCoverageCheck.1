.TH MCOVERAGECHECK 1 "Dec 2016" "Montage 5" "Montage"
.SH NAME
mCoverageCheck \- Filter an image metadata table down to those records representing FITS files that overlap a footprint on the sky

.SH SYNOPSIS
mCoverageCheck [\-s \fIstatusfile\fP] in.tbl out.tbl \-\fImode\fP <parameters>

.SH DESCRIPTION
\fBmCoverageCheck\fP can be used to subset an image metadata table (containing FITS/WCS information or image corners) by determining which records in the table represent images that overlap with a region definition (box or circle in the sky) given on the command line.

.SH OPTIONS
.TP
\-s \fIstatusfile\fP
Output and errors are sent to \fIstatusfile\fP instead of to stdout
.TP
\-\fImode\fP
Can be one of:

points (used to describe a convex polygon)
box
circle
header (area is described in a header file)
point (find images that overlap one particular point on the sky)
cutout (operates like \fIbox\fP mode, but returns metadata that represents what the input images would look like after being subset to exactly match the cutout area)

.TP
in.tbl
Input metadata table.
.TP
out.tbl
Output metadata table, to contain subset of in.tbl.
.TP
\fIparameters\fP
Depends on mode as following:

\fIpoints\fP: \fBra1 dec1 ra2 dec2 ... raN decN\fP (must be at least 3 vertices)
\fIbox\fP: \fBra dec xsize [ysize [rotation]]\fP

where \fIra, dec\fP is the center of the box, and \fIsize\fP is in degrees.
\fIcircle\fP: \fBra dec radius\fP

where \fIra, dec\fP is the center of the circle, and \fIradius\fP is in degrees (default is 0, in which case it's treated like a point search).
\fIpoint\fP: \fBra dec\fP
\fIheader\fP: \fBregion.hdr\fP (path to an ASCII header template that defines the region of interest)
\fIcutout\fP: \fBra dec xsize [ysize]\fP

where \fIra, dec\fP is the center of the cutout area, and \fIsize\fP is in degrees.



.SH RESULT
The output file \fIout.tbl\fP is created, with \fInimages\fP records.

.SH MESSAGES
.TP
OK
[struct stat="OK", count=\fIn\fP]
.TP
ERROR
Cannot open status file: \fIstatusfile\fP
.TP
ERROR
Invalid region definition mode: \fIstring\fP
.TP
ERROR
Too few vertices for region (must be at least three)
.TP
ERROR
Longitude \fIn\fP (\fIstring\fP) cannot be interpreted as a real number
.TP
ERROR
Latitude \fIn\fP (\fIstring\fP) cannot be interpreted as a real number
.TP
ERROR
Failed to find bounding polygon for points
.TP
ERROR
Too few arguments for box or cutout (must at least have center and size)
.TP
ERROR
Center RA string (\fIstring\fP) cannot be interpreted as a real number
.TP
ERROR
Center Dec string (\fIstring\fP) cannot be interpreted as a real number
.TP
ERROR
X box size string (\fIstring\fP) cannot be interpreted as a real number
.TP
ERROR
X box size (\fIstring\fP) must be a positive number
.TP
ERROR
Y box size string (\fIstring\fP) cannot be interpreted as a real number
.TP
ERROR
Y box size (\fIstring\fP) must be a positive number
.TP
ERROR
Box rotation string (\fIstring\fP) cannot be interpreted as a real number
.TP
ERROR
Must give header file name
.TP
ERROR
Failed to find bounding polygon for points
.TP
ERROR
Too few arguments for circle (must at least have center)
.TP
ERROR
Circle radius string (\fIstring\fP) cannot be interpreted as a real number
.TP
ERROR
Too few arguments for point (must have coordinates)
.TP
ERROR
Point RA string (\fIstring\fP) cannot be interpreted as a real number
.TP
ERROR
Point Dec string (\fIstring\fP) cannot be interpreted as a real number
.TP
ERROR
Error opening table \fIfilename\fP
.TP
ERROR
Cannot create output file \fIfilename\fP
.TP
ERROR
Need either WCS or corner columns.
.TP
ERROR
Failed to create wcs structure for record \fIn\fP

.SH EXAMPLES
.PP
As input, we use this table of 2MASS atlas images.  The images in this table are in two "clumps" on the sky: 3 overlapping images separated by several degrees from the other 12 overlapping images.  Here we will show several ways of isolating the group of 3 images.
.TP
$ mCoverageCheck images.tbl circle.tbl \-circle 262.78208 \-35.03049 1.0
[struct stat="OK", count="3"]
.TP
$ mCoverageCheck images.tbl box.tbl \-box 262.84948 \-34.81197 1.0
[struct stat="OK", count="3"]
.TP
$ mCoverageCheck images.tbl points.tbl \-points 263.3796 \-34.48881 262.15127 \-34.51032 262.90267 \-35.38753
[struct stat="OK", count="3"]
.TP
$ mCoverageCheck images.tbl point.tbl \-point 262.78208 \-35.03049
[struct stat="OK", count="3"]
.PP
Each of these methods results in this output table containing only 3 records out of the original 15.

.SH BUGS
The drizzle algorithm has been implemented but has not been tested
in this release.
.PP
If a header template contains carriage returns (i.e., created/modified
on a Windows machine), the cfitsio library will be unable to read it
properly, resulting in the error: [struct stat="ERROR", status=207,
msg="illegal character in keyword"]
.PP
It is best for the background correction algorithms if the area
described in the header template completely encloses all of the input
images in their entirety. If parts of input images are "chopped off"
by the header template, the background correction will be affected. We
recommend you use an expanded header for the reprojection and
background modeling steps, returning to the originally desired header
size for the final coaddition. The default background matching assumes
that there are no non-linear background variations in the individual
images (and therefore in the overlap differences). If there is any
uncertainty in this regard, it is safer to turn on the "level only"
background matching (the "\-l" flag in mBgModel.

.SH COPYRIGHT
2001-2015 California Institute of Technology, Pasadena, California
.PP
If your research uses Montage, please include the following
acknowledgement: "This research made use of Montage. It is funded by
the National Science Foundation under Grant Number ACI-1440620, and
was previously funded by the National Aeronautics and Space
Administration's Earth Science Technology Office, Computation
Technologies Project, under Cooperative Agreement Number NCC5-626
between NASA and the California Institute of Technology."
.PP
The Montage distribution includes an adaptation of the MOPEX algorithm
developed at the Spitzer Science Center.
