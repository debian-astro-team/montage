.TH MHDRTBL 1 "Dec 2016" "Montage 5" "Montage"
.SH NAME
mHdrtbl \- Generate metadata from a set of header files

.SH SYNOPSIS
mHdrtbl [\-rcdb] [\-s \fIstatusfile\fP] [\-t \fIimglist\fP] directory images.tbl

.SH DESCRIPTION
\fBmHdrtbl\fP operates in a fashion similar to \fBmImgtbl\fP, but is used on a set of header template files instead of FITS images.

.SH OPTIONS
.TP
\-r
\fBmHdrtbl\fP can also be used as a standalone program to gather image metadata for other purposes (to populate a database, as a basis for spatial coverage searches, etc.)   In this case it is often desirable to collect information on all the files in a directory tree recursively.  The "\-r" (recursive) flag instructs \fBmHdrtbl\fP to search the given directory and all its subdirectories recursively.
.TP
\-c
The "\-c" (corners) option in mHdrtbl will cause eight extra columns to be added to the output metadata table containing the RA, Dec coordinates (ra1, dec1, ... ra4, dec4) of the image corners.  The output is always Equatorial J2000, even if the input is some other system.  This has been done to make the metadata uniform so that it can easily be used for coverage searches, etc.  The "\-c" option is not needed for normal Montage processing.
.TP
\-d
Turn on debugging
.TP
\-b
When this switch is set, \fBmHdrtbl\fP will explicitly output each header file it finds that does not appear to be valid, along with information on the error.
.TP
\-s \fIstatusfile\fP
Output and errors are written to \fIstatusfile\fP instead of being written to stdout.
.TP
\-t \fIimglist\fP
\fBmHdrtbl\fP will only process files with names specified in table \fIimglist\fP, ignoring any other files in the directory. Example: example.imglist.

.SH ARGUMENTS
.TP
directory
Path to directory containing set of input header templates.

.TP
images.tbl
Path of output metadata table.

.SH RESULT
Output table contains metadata information from all readable header templates in the directory specified by \fIdirectory\fP.  \fIcount\fP is the number of records in the table, and \fIbadhdrs\fP is the number of files for which FITS/WCS metadata could not be extracted.

.SH OUTPUT COLUMNS
.TP
cntr
A unique counter (row number)
.TP
ra,dec
Image position in decimal degree format
.TP
cra,cdec
Right ascension in HHMMSS.SS format. Declination in DDMMSS.S format
.TP
naxis1, naxis2
The size of the image in pixels for dimensions 1 and 2
.TP
ctype1, ctype2
The coordinate system (the first four characters) and WCS map projection (last three characters) for dimensions 1 and 2
.TP
crpix1, crpix2
The pixel coordinates of the reference location (can be fractional and can be off the image) for dimensions 1 and 2
.TP
crval1, crval2
The coordinates of a reference location on the sky (often at the center of the image) for dimensions 1 and 2
.TP
cdelt1, cdelt2
The pixel scale (in degrees on the sky per pixel) at the reference location for dimensions 1 and 2
.TP
crota2
The rotation angle from the "up" direction to the celestial pole
.TP
equinox
Precessional year associated with the coordinate system
.TP
hdu
Numerical identifier for the FITS extension that the image info is extracted from, for FITS files with multiple HDUs.
.TP
size
Filesize (in bytes) of the input FITS file
.TP
fname
The path to the input FITS file

.SH MESSAGES
.TP
OK
[struct stat="OK", count=\fIcount\fP, badfits=\fIbadfits\fP]
.TP
ERROR
Illegal argument: \-\fIarg\fP
.TP
ERROR
Cannot open status file: \fIstatusfile\fP
.TP
ERROR
Cannot open field list file: \fIfieldlistfile\fP
.TP
ERROR
Cannot open image list file: \fIimgfile\fP
.TP
ERROR
Image table needs column fname/file
.TP
ERROR
Illegal field name: \fIstring\fP]
.TP
ERROR
Illegal field type: \fIstring\fP]
.TP
ERROR
Cannot access \fIdirectory\fP
.TP
ERROR
\fIdirectory\fP is not a directory
.TP
ERROR
Can't open output table.
.TP
ERROR
Can't open copy table.
.TP
ERROR
Can't open tmp (in) table.
.TP
ERROR
Can't open tmp (out) table.
.TP
ERROR
Can't open final table.
.TP
ERROR
\fIFITS library error\fP

.SH EXAMPLES
.PP
The following example runs \fBmHdrtbl\fP on a directory containing header templates stripped from 14 2MASS images:
.TP
$ mHdrtbl input/2mass headers.tbl
[struct stat="OK", count=14, badfits=0]
.PP
Output: \fBheaders.tbl\fP.

.SH BUGS
The drizzle algorithm has been implemented but has not been tested
in this release.
.PP
If a header template contains carriage returns (i.e., created/modified
on a Windows machine), the cfitsio library will be unable to read it
properly, resulting in the error: [struct stat="ERROR", status=207,
msg="illegal character in keyword"]
.PP
It is best for the background correction algorithms if the area
described in the header template completely encloses all of the input
images in their entirety. If parts of input images are "chopped off"
by the header template, the background correction will be affected. We
recommend you use an expanded header for the reprojection and
background modeling steps, returning to the originally desired header
size for the final coaddition. The default background matching assumes
that there are no non-linear background variations in the individual
images (and therefore in the overlap differences). If there is any
uncertainty in this regard, it is safer to turn on the "level only"
background matching (the "\-l" flag in mBgModel.

.SH COPYRIGHT
2001-2015 California Institute of Technology, Pasadena, California
.PP
If your research uses Montage, please include the following
acknowledgement: "This research made use of Montage. It is funded by
the National Science Foundation under Grant Number ACI-1440620, and
was previously funded by the National Aeronautics and Space
Administration's Earth Science Technology Office, Computation
Technologies Project, under Cooperative Agreement Number NCC5-626
between NASA and the California Institute of Technology."
.PP
The Montage distribution includes an adaptation of the MOPEX algorithm
developed at the Spitzer Science Center.
