.TH MPROJECT 1 "Dec 2016" "Montage 5" "Montage"
.SH NAME
mProject \- Re-project FITS images

.SH SYNOPSIS
mProject [\-z \fIfactor\fP] [\-d \fIlevel\fP] [\-s \fIstatusfile\fP] [\-h \fIhdu\fP] [\-x \fIscale\fP] [\-w \fIweightfile\fP] [\-W \fIfixed-weight\fP] [\-t \fIthreshold\fP] [\-X] [\-b \fIborder-string\fP] [\-e(nergy-mode)] [\-f] in.fits out.fits hdr.template

.SH DESCRIPTION
\fBmProject\fP reprojects a single image to the scale defined in a FITS header template file (read more about header templates here). The program produces a pair of images: the reprojected image and an "area" image consisting of the fraction input pixel sky area that went into each output pixel.  The "drizzle" algorithm is implemented.  The algorithm proceeds by mapping pixel corners (as adjusted by drizzle, if called) from the input pixel space to the output pixel space, calculating overlap area with each output pixel, and accumulating an appropriate fraction of the input flux into the output image pixels.  In addition, the appropriate fraction of the input pixel area is accumulated into the area image pixels.  Projection of points from input pixel space to output pixel space is calculated in two steps: first map from input pixel space to sky coordinates; second map from sky coordinates to output pixel space.

One situation that has happened often enough to warrant special note:  It is possible to define a FITS header with the
reference location off the image.  In particular, people often reference cylindrical projections (\fIe.g.,\fP CAR, CEA)
to location (0.,0.) (\fIe.g.,\fP the Galactic center).  This is not wrong in itself but does lead to extremely large
pixel offsets (\fIe.g.,\fP in the hundreds of thousands).  Related to this, if you extract a header from an image with
large offsets of this type, it is unlikely that you can simply change projection types without also adjusting the reference
location/offsets.  Most likely, you will end up with the reproject data all being off-scale.


.SH OPTIONS
.TP
\-z \fIfactor\fP
Processing is done utilizing the drizzle algorithm. \fIfactor\fP is a floating point number; recommended drizzle factors are from 0.5 to 1.
.TP
\-d \fIlevel\fP
Causes additional debugging information to be printed to stdout.  Valid levels are 1\-5 (for higher debugging levels, it is recommended to redirect the output to a file).
.TP
\-s \fIstatusfile\fP
Output and errors are written to \fIstatusfile\fP instead of being written to stdout.
.TP
\-h \fIhdu\fP
Use the specified FITS extension (default is to use the first HDU with image data)
.TP
\-x \fIscale\fP
Apply a correction factor of \fIscale\fP to each pixel
.TP
\-w \fIweightfile\fP
Path to a weight map to be used when reading values from the input image.
.TP
\-W \fIfixed-weight\fP
Use constant weight value for the whole image.
.TP
\-t \fIthreshold\fP
Pixels with weights below \fIthreshold\fP will be treated as blank.
.TP
\-X
Makes the output region (originally defined in the header template) big enough to include all of the input images
.TP
\-b \fIborder-string\fP
Define a border width or border pixel corners outside of which all pixels are set to NaN.
.TP
\-e
Process the data as total energy rather than the default energy density (scale values by relative pixel areas).
.TP
\-f
Output the full region requested even if there is a border of NULL values (default is to shrinkwrap to just the pixels with values).

.SH ARGUMENTS
.TP
in.fits
Input FITS file to be reprojected.
.TP
out.fits
Path of output FITS file to be created.
.TP
hdr.template
FITS header template to be used in generation of output image

.SH RESULT
Two files are created as output: the reprojected FITS file (\fIout.fits\fP), and an "area" image (\fIout_area.fits\fP) which records the amount of coverage (in output pixel space) provided by the input images.

.SH MESSAGES
.TP
\fIOK\fP
[struct stat="\fIOK\fP", time=\fIseconds\fP]
.TP
ERROR
Drizzle factor string (\fIstring\fP) cannot be interpreted as a real number
.TP
ERROR
Weight threshold string (\fIstring\fP) cannot be interpreted as a real number
.TP
ERROR
Flux scale string (\fIstring\fP) cannot be interpreted as a real number
.TP
ERROR
Cannot open status file: \fIstatusfile\fP
.TP
ERROR
HDU value (\fIhdu\fP) must be a non-negative integer
.TP
ERROR
No overlap
.TP
ERROR
Not enough memory for output data image array
.TP
ERROR
Not enough memory for output area image array
.TP
ERROR
Output wcsinit() failed.
.TP
ERROR
Input wcsinit() failed.
.TP
ERROR
FITS library error
.TP
ERROR
All pixels are blank
.TP
ERROR
Template file not found
.TP
ERROR
Image file \fIin.fits\fP missing or invalid FITS
.TP
ERROR
Weight file \fIweightfile\fP missing or invalid FITS

.SH EXAMPLES
.TP
$ mProject rawdir/real_orig.fits projdir/base_unity.fits templates/galactic_orig.txt
[struct stat="OK", time=143]

.SH BUGS
The drizzle algorithm has been implemented but has not been tested
in this release.
.PP
If a header template contains carriage returns (i.e., created/modified
on a Windows machine), the cfitsio library will be unable to read it
properly, resulting in the error: [struct stat="ERROR", status=207,
msg="illegal character in keyword"]
.PP
It is best for the background correction algorithms if the area
described in the header template completely encloses all of the input
images in their entirety. If parts of input images are "chopped off"
by the header template, the background correction will be affected. We
recommend you use an expanded header for the reprojection and
background modeling steps, returning to the originally desired header
size for the final coaddition. The default background matching assumes
that there are no non-linear background variations in the individual
images (and therefore in the overlap differences). If there is any
uncertainty in this regard, it is safer to turn on the "level only"
background matching (the "\-l" flag in mBgModel.

.SH COPYRIGHT
2001-2015 California Institute of Technology, Pasadena, California
.PP
If your research uses Montage, please include the following
acknowledgement: "This research made use of Montage. It is funded by
the National Science Foundation under Grant Number ACI-1440620, and
was previously funded by the National Aeronautics and Space
Administration's Earth Science Technology Office, Computation
Technologies Project, under Cooperative Agreement Number NCC5-626
between NASA and the California Institute of Technology."
.PP
The Montage distribution includes an adaptation of the MOPEX algorithm
developed at the Spitzer Science Center.
